<?php

require_once('vendor/autoload.php');

$dotenv = new \Dotenv\Dotenv(__DIR__ . DIRECTORY_SEPARATOR . 'config');
$dotenv->overload();

$dotenv->required([
    'DEPLOY_HOST', 'DEPLOY_HOST_USERNAME', 'DEPLOY_PRIVATE_KEY_PATH', 'DEPLOY_PATH', 'DEPLOY_GIT_PASSWORD',
]);

$config = [
    'host' => getenv('DEPLOY_HOST'),
    'hostUsername' => getenv('DEPLOY_HOST_USERNAME'),
    'privateKeyPath' => getenv('DEPLOY_PRIVATE_KEY_PATH'),
    'deployPath' => getenv('DEPLOY_PATH'),
    'dirName' => null,
    'gitRepositoryServer' => 'gitlab.com',
    'gitRepositoryName' => 'helmes-task',
    'gitUsername' => 'infotechnohelp',
    'gitPassword' => getenv('DEPLOY_GIT_PASSWORD'),
    'copyFiles' => [
        ['config/.env.deploy', 'config/.env',],
    ],
    'permissions' => [
        ['777', 'bin/cake'],
    ],
];

$postCloneCommands = [
    'composer install --no-progress --no-suggest --no-interaction --no-dev',
];

$ignoreBackup = (count($argv) > 1) ? filter_var($argv[1], FILTER_VALIDATE_BOOLEAN) : false;

(new \Infotechnohelp\Deployer\Deployer($config))->deploy($postCloneCommands, $ignoreBackup);