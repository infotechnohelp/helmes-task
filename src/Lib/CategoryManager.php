<?php

namespace App\Lib;

use App\Model\Entity\Category;
use App\Model\Table\CategoriesTable;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;

class CategoryManager
{
    private function getMain(): ResultSet
    {
        /** @var CategoriesTable $CategoriesTable */
        $CategoriesTable = TableRegistry::getTableLocator()->get('Categories');

        return $CategoriesTable->find()->where(['parent_id IS NULL'])->all();
    }

    private function getChildren(int $parentId): array
    {
        /** @var CategoriesTable $CategoriesTable */
        $CategoriesTable = TableRegistry::getTableLocator()->get('Categories');

        $ResultSet = $CategoriesTable->find()->where(['parent_id' => $parentId])->all();

        $result = [];

        /** @var Category $Category */
        foreach ($ResultSet as $Category){
            $result[$Category->get('title')] = [
                'id' => $Category->get('id'),
                'children' => $this->getChildren($Category->get('id')),
            ];
        }

        return $result;
    }

    public function getTree(): array
    {
        $result = [];

        /** @var Category $mainCategory */
        foreach ($this->getMain() as $mainCategory) {
            $result[$mainCategory->get('title')] = [
                'id' => $mainCategory->get('id'),
                'children' => $this->getChildren($mainCategory->get('id')),
            ];
        }

        return $result;
    }
}