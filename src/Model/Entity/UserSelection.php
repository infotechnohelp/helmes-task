<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSelection Entity
 *
 * @property int $id
 * @property string $name
 * @property string $selection_json
 * @property bool $agreed
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class UserSelection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'selection_json' => true,
        'agreed' => true,
        'created' => true,
        'modified' => true
    ];
}
