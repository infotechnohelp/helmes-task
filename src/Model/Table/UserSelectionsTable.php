<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSelections Model
 *
 * @method \App\Model\Entity\UserSelection get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserSelection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserSelection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserSelection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSelection|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSelection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserSelection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserSelection findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserSelectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_selections');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 191)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('selection_json')
            ->requirePresence('selection_json', 'create')
            ->allowEmptyString('selection_json', false)
            ->add('selection_json', 'selectionRequired', [
                'rule' => function ($data) {

                    if (empty(json_decode($data, true))) {
                        return 'Please choose at least one option from selection list';
                    }

                    return true;
                },

            ]);

        $validator
            ->boolean('agreed')
            ->requirePresence('agreed', 'create')
            ->add('agreed', 'termsAgreed', [
                'rule' => function ($data) {

                    if (!$data) {
                        return 'Terms are not agreed';
                    }
                    return true;
                },
            ]);

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }
}
