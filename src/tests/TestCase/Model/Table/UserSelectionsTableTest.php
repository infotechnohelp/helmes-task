<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserSelectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserSelectionsTable Test Case
 */
class UserSelectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserSelectionsTable
     */
    public $UserSelections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserSelections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserSelections') ? [] : ['className' => UserSelectionsTable::class];
        $this->UserSelections = TableRegistry::getTableLocator()->get('UserSelections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserSelections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
