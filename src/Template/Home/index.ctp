<?php

if (isset($errors)) { ?>

    <h1 style="color:red">Errors occurred while saving: </h1>

    <pre>
        <?php print_r($errors) ?>
    </pre>
<?php } ?>

<?php if (isset($savedResult)) { ?>

    <h1 style="color:green">Selections are saved to DB </h1>
<?php } ?>

<h1>Please enter your name and pick the Sectors you are currently involved in.</h1>

<?php

/** @var \App\Model\Table\UserSelectionsTable $UserSelectionsTable */
$UserSelectionsTable = \Cake\ORM\TableRegistry::getTableLocator()->get('UserSelections');

if ($this->getRequest()->getSession()->read('latest_entity_id')) {

    /** @var \App\Model\Entity\UserSelection $UserSelection */
    $UserSelection = $UserSelectionsTable->findById($this->getRequest()->getSession()->read('latest_entity_id'))->first();
}

if (!isset($UserSelection) && !empty($_POST)) {

    $entityDoesNotExist = true;

    $UserSelection = $UserSelectionsTable->newEntity([
        'name' => $_POST['name'] ?? '',
        'selection_json' => json_encode($_POST['selected-options'] ?? []),
        'agreed' => (array_key_exists('agreed', $_POST)) ? true : false,
    ]);
}
?>

<form action="" method="post">

    <label> Name:</label>
    <input type="text" name="name" value="<?php
    if (isset($UserSelection)) {
        echo $UserSelection->get('name');
    }
    ?>">

    <br/><br/>

    <select name="selected-options[]" multiple>

        <?php

        function printOptions(array $data, int $space, array $selectedOptions = [], bool $firstLevel = true)
        {

            foreach ($data as $key => $value) {
                echo sprintf('<option value="%s" %s>%s%s</option>',
                    $value['id'],
                    in_array((string)$value['id'], $selectedOptions, true) ? 'selected' : '',
                    str_repeat('&nbsp;', 4 * $space),
                    $key);

                if (count($value['children']) > 0) {
                    $newSpace = $space + 1;
                    printOptions($value['children'], $newSpace, $selectedOptions, false);
                }

                if ($firstLevel) {
                    $space = 0;
                }

            }
        }

        $selectedOptions = [];

        if (isset($UserSelection)) {
            $selectedOptions = json_decode($UserSelection->get('selection_json'));
        }

        if($selectedOptions === null){
            $selectedOptions = [];
        }

        printOptions($categories, 0, $selectedOptions);
        ?>
    </select>


    <br/>

    <input name="agreed" type="checkbox" <?php if (isset($UserSelection) && $UserSelection->get('agreed')) {
        echo 'checked';
    } ?>> Agree to terms

    <br/>


    <?php $submitValue = (isset($UserSelection) && $UserSelection !== null && !isset($entityDoesNotExist)) ? 'Update' : 'Save'; ?>

    <input type="submit" value="<?= $submitValue ?>">

    <?php if (isset($UserSelection) && $UserSelection !== null && !isset($entityDoesNotExist)) { ?>
        <input name="update" hidden>
        <input name="saved-name" value="<?= $UserSelection->get('name') ?>" hidden>
    <?php } ?>

</form>

<button><a style="text-decoration: none;color: inherit" href="home/destroy-session">Destroy session</a></button>

