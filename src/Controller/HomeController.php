<?php

namespace App\Controller;

use App\Lib\CategoryManager;
use App\Model\Entity\UserSelection;
use App\Model\Table\UserSelectionsTable;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;

class HomeController extends Controller
{
    private function saveEntity(array $postData)
    {
        /** @var UserSelectionsTable $UserSelectionsTable */
        $UserSelectionsTable = TableRegistry::getTableLocator()->get('UserSelections');

        $UserSelection = $UserSelectionsTable->newEntity([
            'name' => $postData['name'],
            'selection_json' => json_encode($postData['selected-options'] ?? []),
            'agreed' => (array_key_exists('agreed', $postData) && $postData['agreed'] === 'on') ? true : false,
        ]);

        return [$UserSelectionsTable->save($UserSelection), $UserSelection->getErrors()];
    }

    private function updateEntity(array $postData)
    {
        /** @var UserSelectionsTable $UserSelectionsTable */
        $UserSelectionsTable = TableRegistry::getTableLocator()->get('UserSelections');

        /** @var UserSelection $UserSelection */
        $UserSelection = $UserSelectionsTable->findByName($postData['saved-name'])->first();

        $UserSelection->set('name', $postData['name']);

        $UserSelection->set('selection_json', json_encode($postData['selected-options'] ?? []));

        $UserSelection->set('agreed', (array_key_exists('agreed', $postData) && $postData['agreed'] === 'on') ? true : false);

        $validationErrors = $UserSelectionsTable->getValidator()->errors($UserSelection->toArray(), false);

        if (count($validationErrors) > 0) {
            return [false, $validationErrors];
        }

        return [$UserSelectionsTable->save($UserSelection), $UserSelection->getErrors()];
    }

    public function index()
    {
        $this->set('categories', (new CategoryManager())->getTree());

        $postData = $this->getRequest()->getData();


        if (!empty($postData)) {

            $result = null;
            $errors = null;

            if (!array_key_exists('update', $postData)) {
                list($result, $errors) = $this->saveEntity($postData);
            } else {
                list($result, $errors) = $this->updateEntity($postData);
            }

            if ($result) {
                $this->set('savedResult', true);
                $this->getRequest()->getSession()->write('latest_entity_id', $result->get('id'));
                return;
            }

            $this->set('errors', $errors);
        }
    }

    public function destroySession()
    {
        $this->getRequest()->getSession()->destroy();

        $this->redirect('/');
    }
}